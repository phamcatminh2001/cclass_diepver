#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
int sumOfArray(int array[], int size);
int sumOfEven(int array[], int size);
int sumOfOdd(int array[], int size);
int sumOfOddIndex(int array[], int size);
void reverseArray(int array[], int size);
int sumOfEvenIndexReverse(int array[], int size);
int sumOfOddIndexReverse(int array[], int size);
int main()
{
    int array[MAX];
    int size;
    inputArray(array,&size);
    printf("\nTong cac so trong mang la %d",sumOfArray(array,size));
    printf("\nTong cac so chan la %d", sumOfEven(array,size));
    printf("\nTong cac so le la %d", sumOfOdd(array,size));
    printf("\nTong cac so o vi tri le la %d", sumOfOddIndex(array,size));
    printf("\nSau khi dao chieu mang");
    reverseArray(array,size);
    outputArray(array,size);
    printf("\nTong cac so o vi tri chan tu phai qua trai la %d", sumOfEvenIndexReverse(array,size));
    printf("\nTong cac so o vi tri le tu phai qua trai la %d",sumOfOddIndexReverse(array,size));
    printf("\nMang ban dau nhap vao la: ")
    outputArray(array,size);
    return 0;
}
void inputArray(int array[], int* size){
    printf("\nNhap khoang ban can: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
     for(int i = 0; i <= size - 1; i++){
        printf("%4d", array[i]);
     }
}
int sumOfArray(int array[], int size){
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        result = result + array[i];
    }
    return result;
}
int sumOfEven(int array[], int size){
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        if(array[i] % 2 == 0){
            result = result + array[i];
        }
    }
    return result;
}
int sumOfOdd(int array[], int size){
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        if(array[i] % 2 != 0){
            result = result + array[i];
        }
    }
    return result;
}
int sumOfOddIndex(int array[], int size){
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        if(i % 2 != 0){
            result = result + array[i];
        }
    }
    return result;
}
void reverseArray(int array[], int size){
    int arrayTMP[MAX];
    int sizeTMP = 0;
    for(int i = size - 1; i >= 0; i--){
        arrayTMP[sizeTMP] = array[i];
        sizeTMP++;
    }
    for(int i  = 0 ; i<= size - 1;i++){
        array[i] = arrayTMP[i];
    }
}
int sumOfEvenIndexReverse(int array[], int size){
    reverseArray(array,size);
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        if(i % 2 == 0){
            result = result + array[i];
        }
    }
    return result;
    reverseArray(array,size);
}
int sumOfOddIndexReverse(int array[], int size){
    reverseArray(array,size);
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        if(i % 2 != 0){
            result = result + array[i];
        }
    }
    return result;
    reverseArray(array,size);
}
