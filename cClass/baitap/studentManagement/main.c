#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const int MAX = 100;
void printList(void);
void addStudent(char id[][MAX],char name[][MAX], float mark[], int *size );
void printInfo(char id[][MAX],char name[][MAX], float mark[], int size);
int findStudent(char id[][MAX], int size);
void showInfo(char id[][MAX],char name[][MAX], float mark[], int pos);
int updateInfo(char id[][MAX],char name[][MAX], float mark[], int pos);
int deleteInfo(char id[][MAX],char name[][MAX], float mark[], int pos, int* size);
int main()
{
    int choose;
    char buffer;
    char id[MAX][10];
    char name[MAX][40];
    float mark[MAX];
    int size = 0;

    do{
        do{
            printList();
            printf("\nNhap vao lua chon cua ban: ");
            scanf("%d", &choose);
            scanf("%c", &buffer);
            fflush(stdin);
            if(buffer != 10){
                printf("\nVui long nhap vao so nguyen dich thuc");
            }
        }while(buffer != 10);
        switch(choose){
            case 1:{
                printf("\n1. them sinh vien");
                addStudent(id,name,mark,&size);
                break;
            }
            case 2:{
                printf("\n2. xem danh sach sinh vien");
                printInfo(id,name,mark,size);
                break;
            }
            case 3:{
                printf("\n3. Tim kiem sinh vien bang ID");
                int pos = findStudent(id,size);
                if(pos == -1){
                    printf("\nKhong tim duoc sinh vien");
                }else{
                    printf("\nSinh vien da tim duoc la: ");
                    showInfo(id,name,mark,pos);
                }
                break;
            }
            case 4:{
                printf("\n4. Chinh sua thong tin");
                int pos = findStudent(id,size);
                if(pos == -1){
                    printf("\nKhong tim duoc sinh vien");
                }else{
                    printf("ban dang update thong tin cua sinh vien duoi day");
                    showInfo(id,name,mark,pos);
                    updateInfo(id,name,mark,pos);
                }
                break;
            }
            case 5:{
                printf("\n5. Xoa thong tin");
                int pos = findStudent(id,size);
                if(pos == -1){
                    printf("\nKhong tim duoc sinh vien");
                }else{
                    printf("ban dang xoa thong tin cua sinh vien duoi day");
                    showInfo(id,name,mark,pos);
                    deleteInfo(id,name,mark,pos,&size);
                    printf("\nDanh sach sau khi xoa");
                    printInfo(id,name,mark,size);
                }
                break;
            }
            case 6:{
                printf("\nHen gap lai");
                break;
            }
            default:{
                printf("\n!!Vui long nhap trong khoang tu 1-6!!");
                break;
            }
        }
    }while(choose != 6);

    return 0;
}
void printList(void){
    printf("\nChon mot trong so cac muc sau.");
    printf("\n1. Them sinh vien.");
    printf("\n2. Xem danh sach sinh vien.");
    printf("\n3. Tim kiem sinh vien bang ID.");
    printf("\n4. Chinh sua thong tin.");
    printf("\n5. Xoa thong tin.");
    printf("\n6. Thoat");
}
void addStudent(char id[][MAX],char name[][MAX], float mark[], int *size ){
    printf("\nNhap vao ten sinh vien : ");
    gets(name[*size]);
    fflush(stdin);
    printf("\nNhap vao ID sinh vien : ");
    gets(id[*size]);
    fflush(stdin);
    printf("\nNhap vao diem cua sinh vien : ");
    scanf("%f", &mark[*size]);
    fflush(stdin);
    *size = *size + 1;
}
void printInfo(char id[][MAX],char name[][MAX], float mark[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("\n|%-11s|%-30s|%6.2f|", id[i], name[i], mark[i]);
    }
}
int findStudent(char id[][MAX], int size){
    char key[MAX];
    printf("\nNhap ID sinh vien: ");
    gets(key);
    fflush(stdin);
    for(int i = 0; i <= size - 1; i++){
        if(strcmp(key,id[i]) == 0){
            return i;
        }
    }
    return -1;
}
void showInfo(char id[][MAX],char name[][MAX], float mark[], int pos){
    printf("\n|%-11s|%-30s|%6.2f|", id[pos], name[pos], mark[pos]);
}
int updateInfo(char id[][MAX],char name[][MAX], float mark[], int pos){
    printf("\nNhap vao ten sinh vien : ");
    gets(name[pos]);
    fflush(stdin);
    printf("\nNhap vao ID sinh vien : ");
    gets(id[pos]);
    fflush(stdin);
    printf("\nNhap vao diem cua sinh vien : ");
    scanf("%f", &mark[pos]);
    fflush(stdin);
}
int deleteInfo(char id[][MAX],char name[][MAX], float mark[], int pos, int* size){
    for(int i = pos; i <= *size - 1; i++){
        strcpy(id[i], id[i + 1]);
        strcpy(name[i], name[i + 1]);
        mark[i] = mark[i + 1];
    }
    *size = *size - 1;
}
