#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
void reverseArray(int array[], int size);
int main()
{
    int array[MAX];
    int size;
    inputArray(array,&size);
    printf("\nMang vua nhap la: ");
    outputArray(array,size);
    printf("\nMang sau khi dao chieu: ");
    reverseArray(array,size);
    outputArray(array,size);
    return 0;
}
//nhap array va reverse.
void inputArray(int array[], int* size){
    printf("\nNhap khoang ban can: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]: ", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%4d", array[i]);
    }
}
void reverseArray(int array[], int size){
    int arrayTMP[MAX];
    int sizeTmp = size;
    for(int i = 0; i <= size - 1;i++){
        arrayTMP[i] = array[sizeTmp - 1];
        sizeTmp--;
    }
    for(int i  = 0 ; i<= size - 1;i++){
        array[i] = arrayTMP[i];
    }
}
