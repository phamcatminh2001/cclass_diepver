#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
int sortArray(int array[], int size);
int printOdd(int array[], int size);
int main()
{
    // your program should allow user to find the odd number that appears the most in the array of 7 integer
    // when the odd number appears the most in the array, your program prints out that number,but ascending
    // when there is no odd number existing in the array, your program will print No Odd Number.
    int array[MAX];
    int size;
    inputArray(array,&size);
    printOdd(array,size);

    return 0;
}
void inputArray(int array[], int* size){
    printf("\nBan muon bao nhieu so: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%4d", array[i]);
    }
}
int sortArray(int array[], int size){
    for(int i = 0; i <= size - 2; i++){
        for(int j = i + 1; j <= size - 1; j++){
            if(array[i] > array[j]){
                int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
            }
        }
    }
}
int printOdd(int array[], int size){
    int oddArray[MAX];
    int oddSize = 0;
    for(int i = 0; i <= size - 1; i++){
        if(array[i] % 2 != 0){
            oddArray[oddSize] = array[i];
            oddSize++;
        }
    }if(oddSize == 0){
        printf("\nKhong co so le trong ham");
        return 0;
    }else{
        sortArray(oddArray,oddSize);
        printf("\nCac so le trong ham, tu be den lon la: ");
        outputArray(oddArray,oddSize);
    }
}
