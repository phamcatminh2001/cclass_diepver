#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
int oddInArray(int array[], int size, int arrayTmp[], int* sizeTmp);
int sortArray(int array[], int size);
int main()
{
    int array[MAX];
    int arrayTmp[MAX];
    int size = 0;
    int sizeTmp = 0;
    inputArray(array,&size);
    printf("\nMang vua nhap: ");
    outputArray(array,size);
    printf("\n");


    if(oddInArray(array,size,arrayTmp,&sizeTmp) == 0){
        printf("\nKhong co so le trong mang");
        return 0;
    }else{
        printf("\nSo le trong mang duoc sort the ascending la: ");
        outputArray(arrayTmp,sizeTmp);
    }
    return 0;
}
//nhap array, in ra cac so le trong mang, in theo chieu tang dan.
void inputArray(int array[], int* size){
    printf("\nNhap khoang ban can: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]: ", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%4d", array[i]);
    }
}
int sortArray(int array[], int size){
    for(int i = 0; i <= size - 2; i++){
        for(int j = i + 1; j <= size - 1; j++){
            if(array[i] > array[j]){
                int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
            }
        }
    }
}
int oddInArray(int array[], int size, int arrayTmp[], int* sizeTmp){
    sortArray(array,size);
    int count= 0;
    for(int i = 0; i <= size - 1; i++){
        if(array[i] % 2 != 0){
            arrayTmp[*sizeTmp] = array[i];
            *sizeTmp = *sizeTmp + 1;
            count++;
        }
    }
    return count;
}
