#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int *size);
void outputArray(int array[], int size);
int checkSymetric(int array[], int size);
int main()
{
    int array[MAX];
    int size;
    inputArray(array,&size);
    if(checkSymetric(array,size) == 1){
        printf("\nDay la symetric");
    }else{
        printf("\nDay khong phai Symetric");
    }

    return 0;
}
void inputArray(int array[], int *size){
    printf("\nNhap khoang: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]: ",i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%3d", array[i]);
    }
}

int checkSymetric(int array[], int size){
    int arrayTmp[MAX];
    int sizeTmp = 0;
    for(int i = size - 1; i >= 0; i--){
        arrayTmp[sizeTmp] = array[i];
        sizeTmp++;
    }
    for(int i = 0; i <= size - 1; i++){
        if(array[i] != arrayTmp[i]){
            return 0;
        }
    }
    return 1;
}
//23132
//23132
//

//23123
//3213
