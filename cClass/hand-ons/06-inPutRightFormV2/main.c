#include <stdio.h>
#include <stdlib.h>

int main()
{
    int number;
    char buffer;
    int tmp;
    int result = 0;
    do{
        printf("\nNhap so: ");
        scanf("%d", &number);
        scanf("%c", &buffer);
        fflush(stdin);
        if(buffer != 10){
            printf("\nNGU");
        }
    }while(buffer != 10);
    printf("\nnumber = %d", number);
    //int 1234
    //1234%10 = 4
    //1234/10 = 123
    for(;number != 0;){
        tmp = number % 10;
        result = result + tmp;
        number = number / 10;
    }

    printf("\nResult = %d", result);
    return 0;
}
