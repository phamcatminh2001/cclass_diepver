#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
int inputArray(int array[], int size);
int printfArray(int array[], int size);
int main()
{
   //viet ham
   //nhap mang
   //xuat mang

    int array[MAX];
    int size;
    printf("\nsize : ");
    scanf("%d", &size);
    printf("gia tri cua ten mang: %u", array);
    printf("dia chi cua bien dau tien: %u", &array[0]);
    //tên mảng đang lưu địa chỉ biến đầu tiên
    //=> mà 1 biến lưu địa chỉ => con trõ
    //=> mảng là 1 con trỏ
    //=> pass by reference
    inputArray(array, size);
    printf("\nin ra man hinh: ");
    printfArray( array, size);

    return 0;
}

int inputArray(int array[], int size){
   for(int i = 0; i <= size - 1; i++){
        printf("\narray[%d]: ", i);
        scanf("%d", &array[i]);
   }
}
int printfArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%5d", array[i]);
   }
}
