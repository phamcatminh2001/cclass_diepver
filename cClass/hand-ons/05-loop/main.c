#include <stdio.h>
#include <stdlib.h>

int main()
{
    //for -- loop
    //do while
    //công thức
   /* do{
        statements;
        bước nhảy;
    }while(điều kiện);*/
    int i = 4;
    do{
    printf("\nMinh khong thuoc bai");
        i++;
    }while(i <= 3);
    //do while giống như đi xe bus
    //lên xe, nó lại,
    //mua => ngồi tiếp
    // không mua => xuống xe
    //int j = 4;
    //while(j <= 3){
       // printf("\n Minh khong thuoc bai");
       // j++;
    //}
    return 0;
}
