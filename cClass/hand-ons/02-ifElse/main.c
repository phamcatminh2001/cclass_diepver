#include <stdio.h>
#include <stdlib.h>

int main()
{
    //boolean
    // 1 > 3 : false
    // 3 > 2 : true
    /*
        if(boolean){
            statements;
        }else if(boolean){
            statements;
        }
        else{
            statements;
        }
    */
    int a;
    printf("\nOng oi cho toi xin gia tri bien a nha : ");
    scanf("%d", &a);
    //a là biến
    //&a là địa chỉ của biến a
    printf("a = %d", a);

    if(a > 10){
        printf("\na= %d lon hon 10", a);
    }else if(a < 10){
        printf("\na= %d be hon 10", a);
    }else{
        printf("\na = 10");
    }
    return 0;
}
