#include <stdio.h>
#include <stdlib.h>
void capitalCharV1(void);
void capitalCharV2(char character);
//kiểu 3 là kiểu k vào, có ra
char capitalCharV3(void);
char capitalCharV4(char c);
int main()
{
    /*
    //char key;
    //printf("\nnhap vao di ban oi: ");
    //scanf("%c", &key);
    //capitalCharV2(key);
    //printf("\nngoai ham c = %c", key);
    */

    //printf("\ncharacter = %c", capitalCharV3());
    char character;
    printf("\ncharacter = ");
    scanf("%c", &character);
    //character = capitalCharV4(character); //hàm này return nên nó sẻ k xuất ra màn hình giá trị gỉ hết
    printf("\ncharacter = %c", capitalCharV4(character));
    printf("\ncharacter ngoai main = %c", character);
    return 0;
}
void capitalCharV1(void){
    char c;
    printf("\nNhap vao di ban: ");
    scanf("%c",&c);
    fflush(stdin);
    if(c >= 65 && c <= 90){
        printf("c = %c",c);
    }else{
        c = c - 32;
        printf("\nc = %c", c);
    }
}


void capitalCharV2(char character){
    if(character >= 65 && character <= 90){
        printf("character = %c",character);
    }else{
        character = character - 32;
        printf("\ntrong ham c = %c", character);
    }
}
char capitalCharV3(void){
    //trả về kết quả tức là có return
    char character;
    printf("\ncharacter = ");
    scanf("%c", &character);
    if(character >= 65 && character <= 90){
        return character;
    }else{
        return character - 32;
    }
}
char capitalCharV4(char c){
    if(c >= 65 && c <= 90){
        return c;
    }else{
        return c - 32;
    }
}

