#include <stdio.h>
#include <stdlib.h>

int main()
{
    int tt = 200;
    int *hari = &tt;
    printf("\nGia tri cua tt : %d", tt);
    printf("\ndia chi nha tt : %u", &tt);

    printf("\nGia tri cua hari: %u", hari);
    printf("\nDia chi nha hari: %u", &hari);
    printf("\nGia tri có trong ví tiền của anh tt thông qua co hari: %d", *hari);
    //cô hari xài tiền của tt mà k cần xin phép anh ấy
    *hari = *hari - 200;

    printf("\nGia tri cua tt : %d", tt);
    return 0;
}
//gtri dia chi
