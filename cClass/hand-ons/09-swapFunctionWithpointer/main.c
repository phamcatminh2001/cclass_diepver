#include <stdio.h>
#include <stdlib.h>
void swap(int *number1, int *number2);
int main()
{
    //xay dung ham swap
    //nho rang swap thi thay doi 2 gia tri 2 bien
    //phai tra ra ket qua 2 bien
    // khong co ham nao co the tra ra 2 bien cung luc
    //=> pointer
    int a = 10;
    int b = 30;
    printf("\ntruoc khi dung ham a= %d, b = %d", a, b);
    swap(&a, &b);
    printf("\nngoai main a= %d, b = %d", a, b);
    return 0;
}
//pass by references:truyền tham chiếu
void swap(int *number1, int *number2){
    int tmp;
    tmp = *number1;
    *number1 = *number2;
    *number2 = tmp;

    printf("\nTrong ham a = %d, b = %d", *number1, *number2);
}
