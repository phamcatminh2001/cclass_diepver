#include <stdio.h>
#include <stdlib.h>

int main()
{
    //comment
    /*
    */
    //C : là ngôn ngữ lập trình bậc cao
    // duoc tao ra boi dennis richei
    //1940
    //Tại sao học C:
    //đây là khởi nguồn của hầu hết tất cả ngôn ngử
    //và nó có cấu trúc không lỗi thời
    //ngôn ngữ này giúp cải thiện tư dduy lập trình
    //C được dùng :
    //viết hệ điều hành unix:
    //lập trình nhúng embed
    //driver network:

    //CTF:cature the flag

    //ký tự         a b c '6'   char    character   1byte
    //số nguyên     1 10 6 20   int     integer     4byte
    //số thực       2.3 6.7     float   float       4byte
    //                          double  double      8byte

    //char firstCharOfName = 'M';
    //ọng ram ơi, cho tôi xin 1byte để lưu ký tự M
    //printf("firstCharOfName = %c",firstCharOfName);
    //%c: hằng số char giúp hiển thị ký tự
    //int age = 19;
    //xin 4byte lưu số nguyên 19
    //đổ giá trị 19 vào age
    //printf("\n%d is age of Minh", age);
    //%d : hằng số digit giúp hiển thị số nguyên
    //float point = 6.4;
    // xin 4byte luu so thuc 6.4
    //printf("\n%.1f is point of Minh", point);
    //%f: là hằng số float giúp hiển thị số thực

    //int number = 'a';
    //printf("\nnumber = %d", number);
    //int number2 = 'b';
    //printf("\nnumber2 = %d", number2);
    //char c = 122;
    //printf("\nc = %c",c);


    //bài 1 : nhập vào số nguyên, và tăng số đó lên 2 ,in ra màn hình kết quả
    // I        P       O
    //input     Process Output
    //int number = 21;
    //int result;
    //process
    //result = number + 2;
    //printf("\nresult = %d", number );
    //number = number + 2;
    //number -= 2;
    //number = 21 + 2 = 23;
    //output
    //printf("\nresult = %d", number );
/*
    //bai 2: cho 2 biến chứa giá trị khác nhau là 2 số nguyên,hoán đổi giá trị trong 2 biến, và in ra màn hình .swap
    int a = 10;
    int b = 20;
    //int tmp = 0;
    //áp dụng phép gán
    //làm láo, mất dạy, bẻ gảy tay
    //printf("\n a = %d, b = %d",b,a);
    //a đổ vào tmp
    printf("\n a = %d, b = %d",a,b);
    //tmp = a;
    //a = b;
    //b = tmp;

    a = a + b;  //a = 30
    b = a - b;  //b = 10
    a = a - b;  //a = 20

    printf("\n a = %d, b = %d",a,b);

    /*
        ly nươc 1: string đỏ;
        ly nước 2: redbull;
        tui cần 1 ly rổng
        biến rổng: tmp:temp: tạm bợ
        biến tạm: kỷ thuật cốc nước


*/
    //cho 2 số nguyên , in ra màn hình kết quả của phép chia 2 số số
    float a = 10;
    float b = 3;
    float result = a / b;
    printf("\nresult = %f", result);


    return 0;
}
