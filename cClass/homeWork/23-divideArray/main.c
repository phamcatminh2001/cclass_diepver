#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
int divideArray(int array[], int size,int array1[],int* size1,int array2[],int* size2);
int main()
{
    int array[MAX];
    int size;
    int array1[MAX];
    int size1 = 0;
    int array2[MAX];
    int size2 = 0;
    inputArray(array,&size);
    divideArray(array,size,array1,&size1,array2,&size2);
    outputArray(array1,size1);
    printf("\n");
    outputArray(array2,size2);
    return 0;
}
void inputArray(int array[], int* size){
    printf("\nNhap kich thuoc mang: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
     for(int i = 0; i <= size - 1; i++){
        printf("%5d", array[i]);
     }
}
int divideArray(int array[], int size,int array1[],int* size1,int array2[],int* size2){
    int pos;
    printf("\"nBan muon tach o vi tri nao trong mang: ");
    scanf("%d", &pos);
    if(pos > size - 1){
        printf("\nMang k du dai de tach o vi tri nay");
        return 0;
    }
    for(int i = 0; i<= pos - 1;i++){
        array1[*size1] = array[i];
        *size1 = *size1 + 1;
    }
    for(int i = pos ; i<= size - 1;i++){
        array2[*size2] = array[i];
        *size2 = *size2 + 1;
    }
}
