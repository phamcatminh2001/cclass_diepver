#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
void addElementIntoArray(int array[],int* size);
void deleteElementIntoArray(int array[], int* size);
int main()
{
    int array[MAX];
    int size;
    inputArray(array,&size);
    addElementIntoArray(array,&size);
    printf("\nHam sau khi them phat tu: ");
    outputArray(array,size);
    deleteElementFromArray(array,&size);
    printf("\nHam sau rut phan tu: ");
    outputArray(array,size);

    return 0;
}
void inputArray(int array[], int* size){
    printf("\nNhap kich thuoc mang: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
     for(int i = 0; i <= size - 1; i++){
        printf("%5d", array[i]);
     }
}

//value:    5 4 3 6 1
//index:    0 1 2 3 4 5
//size + 1
//muon add:
//  xin vi tri add(pos) = 2(check xem no co nam trong mang k , neu no be hon k thi fix cho no bang 0, neu no lon size, fix cho bang size)
//  add so bao nhieu(number)= 9
//duyet mang
void addElementIntoArray(int array[],int* size){
    int pos;
    int number;
    printf("\nPos: ");
    scanf("%d", &pos);
    printf("\nNumber: ");
    scanf("%d", &number);

    if(pos > *size){
        pos = *size;
    }else if(pos < 0){
        pos = 0;
    }

    for(int i = *size; i >= pos + 1;i--){
        array[i] = array[i - 1];
    }
    array[pos] = number;

    *size = *size + 1;
}
void deleteElementFromArray(int array[], int* size){
    int pos;
    printf("\nPos: ");
    scanf("%d", &pos);

    if(pos > *size -1){
        pos = *size - 1;
    }else if(pos < 0){
        pos = 0;
    }
    for(int i = pos ; i <= *size - 1; i++){
        array[i] = array[i + 1];
    }
    *size = *size - 1;
}
