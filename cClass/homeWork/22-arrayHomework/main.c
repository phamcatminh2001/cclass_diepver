#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int* size);
void outputArray(int array[], int size);
int sortArray(int array[], int size);
void  printSort(int array[],int size);
int main()
{
    int array[MAX];
    int size;
    inputArray(array,&size);
    printf("\nMang vua nhap: ");
    outputArray(array,size);
    printf("\nin mang theo gia tri tang dan: ");
    //sortArray(array,size);
    printSort(array,size);
    printf("\nMang sau khi sap xep");
    outputArray(array,size);

    return 0;
}
void inputArray(int array[], int* size){
    printf("\nNhap khoang ban muon: ");
    scanf("%d", size);
    for(int i = 0; i <= *size - 1; i++){
        printf("\nArray[%d]: ", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%6d", array[i]);
    }
}
int sortArray(int array[], int size){
    for(int i = 0; i <= size - 2; i++){
        for(int j = i + 1; j <= size - 1; j++){
            if(array[i] > array[j]){
                int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
            }
        }
    }
}

void  printSort(int array[],int size){
    int arrayTMP[MAX];
    for(int i = 0;i<= size - 1; i++){
        arrayTMP[i] = array[i];
    }

    sortArray(arrayTMP,size);
    outputArray(arrayTMP,size);
}
