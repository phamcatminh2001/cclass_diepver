#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Nhập vào 3 số nguyên, sau đó so sánh 3 số này
    //tìm ra số lớn nhất
    //đừng quên có trường hợp bằng nhau
    int a,b,c;
    printf("\na= ");
    scanf("%d", &a);
    printf("\nb= ");
    scanf("%d", &b);
    printf("\nc= ");
    scanf("%d", &c);

    if(a > b && a > c){
        printf("a lon nhat");
    }else if(a>b && a == c){
        printf("a va c lon nhat");
    }else if(a == b && a > c){
        printf(" a va b lon nhat");
    }else if(b > a && b > c){
        printf("b lon nhat");
    }else if(b == a && b > c){
        printf(" b = a lon nhat");
    }else if(b > a && b == c){
        printf("b va c lon nhat");
    }else if(c > a && c > b){
        printf(" c lon nhat");
    }else if(c > a && c == b){
        printf("c = b lon nhat");
    }else if(c == a && c > b){
        printf(" c = a lon nhat");
    }else if(a == b && a == c){
        printf("a = b = c");
    }


    return 0;
}
//nhập vào 3 số
/*
   a > b && a > c : a lớn nhất
   a > b && a == c: a = c lớn nhất
   a == b && a > c: a = b lớn nhất

   b > a && b > c: b lớn nhất
   b == a && b > c : b = a lon nhat
   b > a && b == c : b = c lớn nhất

   c > a && c > b : c lớn nhất
   c > a && c == b : c = b lớn nhất
   c == a && c > b : c = a lớn nhất

   a == b && a == c : a = b = c
*/
