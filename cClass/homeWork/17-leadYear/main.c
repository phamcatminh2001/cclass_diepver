#include <stdio.h>
#include <stdlib.h>
int leadYear(int number);
int main()
{
    // nhap vao 1 nam, kiem tra nam nhuan hay nam thuong.
    // nam nhuan phai la nam chia het cho 4 va dong thoi k chia het cho 100 hoac chia het cho 400
    int year;
    printf("\nNam ");
    scanf("%d", &year);
    if(leadYear(year) == 1){
        printf("\nDay la nam nhuan ");
    }else{
        printf("\nDayla nam thuong");
    }
    return 0;
}
int leadYear(int number){
    if((number % 4 == 0 && number % 100 != 0) || (number % 400 == 0)){
        return 1;
    }else{
        return 0;
    }
}
