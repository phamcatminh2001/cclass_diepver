#include <stdio.h>
#include <stdlib.h>
#include <math.h>
const int MAX = 100;
void printMenu(void);
int inputArray(int array[], int size);
int sumofArray(int array[], int size);
int fatorial(int number);
int checkPrime(int number);
int leadYear(int number);
int main()
{
    //tao ra 1 chuong trinh toan hoc, voi giao dien nhu sau:
    //xuat hien bang menu chon tinh nang:
    //1. tinh tong cac so

    //2. tinh giai thua cua 1 so
    //3. ktra so ngto => chia het cho 1 va chinh no
    //4. ktra nam nhuan => (chia het cho 400) or (vua chia het cho 4 va k chia het cho 100)
    //5. quit
    //yc: khi ng dung chon tren menu, thi ng dung phai nhap so nguyen, neu ng dung nhap so khac menu => nhap lai.
    //khi chon so thi buoc vao ham tuong ung voi so do.
    //khi ham chay xong, thi quay lai menu => dung tiep.
    //chi dung lai khi ng dung chon quit.
    //NOTE: LAM  BAI XONG NHO UONG THUOC DAU DAU!!


    int choose;
    char buffer;
    int array[MAX];
    int size;
    int number;
    int year;
    do{
        printMenu();
        do{
            printf("\nChon 1-5: ");
            scanf("%d", &choose);
            scanf("%c", &buffer);
            fflush(stdin);
            if(buffer != 10){
                printf("\nBan Ngu Vai *beef*");
            }
        }while(buffer != 10);
        //người dùng nhập đúng
        switch(choose){
            case 1:{
                printf("\n1.Tinh tong cac so:");
                //hàm bỏ vào đây
                printf("\nBan muon cong bao nhieu so: ");
                scanf("%d", &size);
                inputArray(array, size);
                printf("\nTong cac so ban vua nhap la: %d",sumofArray(array, size));
                break;
            }
            case 2:{
                printf("\n2. Tinh giai thua cua 1 so:");
                printf("\nNhap so ban muon: ");
                scanf("%d", &number);

                printf("\nGiai thua cua %d la : %d", number, fatorial(number));
                break;
            }
            case 3:{
                printf("\n3.Kiem tra so nguyen to");
                printf("\nNhap so ban oi: ");
                scanf("%d", &number);
                if(checkPrime(number) == 1){
                    printf("\n%d la so nguyen to", number);
                }else{
                    printf("\n%d khong phai so nguyen to", number);
                }
                break;
            }
            case 4:{
                printf("\n4.Kiem tra nam nhuan");
                printf("\nNam ");
                scanf("%d", &year);
                if (leadYear(year) == 1){
                    printf("\nDay la nam nhuan");
                }else{
                    printf("\nDay khong phai nam nhuan");
                }
                break;
            }
            case 5:{
                printf("\nHen Gap Lai");
                break;
            }
            default:{
                printf("\nBan oi chi duoc nhap tu 1- 5 hoi");
            }
        }
    }while(choose != 5);
    return 0;
}
void printMenu(void){
    printf("\n1. Tinh tong cac so");
    printf("\n2. Tinh giai thua cua 1 so");
    printf("\n3. Kiem tra so nguyen to");
    printf("\n4. Kiem tra nam nhuan");
    printf("\n5. Thoat");

}
int inputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("\narray[%d]: ", i);
        scanf("%d", &array[i]);
    }
}
int sumofArray(int array[], int size){
    int result = 0;
    for(int i = 0; i <= size - 1; i++){
        result = result + array[i];
    }
    //chạy hết for tương đương với chạy hết màng
    return result;
}
//4->1234
int fatorial(int number){
    int result = 1;
    for(int i = 1; i <= number; i++){
        result = result * i;
    }
    return result;
}
int checkPrime(int number){
    if(number >= 2){
        for(int i = 2; i <= number - 1; i++){
            if(number % i == 0){
                return 0;
            }
        }
        return 1;
    }else{
        return 0;
    }
}
int leadYear(int number){
    if(((number % 4 == 0) && (number % 100 != 0)) || (number % 400 == 0)){
        return 1;
    }else{
        return 0;
    }
}
