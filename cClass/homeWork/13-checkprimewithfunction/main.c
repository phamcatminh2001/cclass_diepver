#include <stdio.h>
#include <stdlib.h>
int checkPrime(int number);
int main()
{
 //cho mot khoang tu start den end, kiem tra trong khoang do co bao nhieu so ng to, in ra so ng to co trg khoang và sử hàm checkprime de làm bài
    int start, end;
    printf("\nKhoi dau: ");
    scanf("%d", &start);
    printf("\nKet thuc: ");
    scanf("%d", &end);
    int count = 0;

    if(start > end){
        int tmp;
        tmp = start;
        start = end;
        end = tmp;
    }
    for(int i = start; i <= end; i++){
        if(checkPrime(i) == 1){
            printf("%3d", i);
            count++;
        }
    }
    printf("\nCo %d so nguyen to", count);
    return 0;
}
int checkPrime(int number){
    if( number >= 2){
        for(int i = 2; i <= number - 1; i++){
            if(number % i == 0){
                return 0;
            }
        }
        return 1;
    }else{
        return 0;
    }
}
