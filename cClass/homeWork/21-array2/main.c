#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[], int size);
void outputArray(int array[], int size);
int comparedArray(int array1[], int size1, int array2[], int size2);
int compineArray(int array1[], int* size1, int array2[], int size2);
int compineArrayV2(int array1[], int size1, int array2[], int size2,int totalArray[],int *totalSize);
int sortArray(int array[], int size);
int main()
{
    //nhap vao 2 mang
    //so sanh 2 mang
    //gop 2 mang thanh 1 mang
    //tim min max cua mang
    int array1[MAX], array2[MAX];
    int size1, size2;
    int totalArray[MAX];
    int totalSize = 0;
    printf("\nNhap khoang ban muon di: ");
    scanf("%d", &size1);
    inputArray(array1,size1);
    printf("\nNhap khoang ban muon lan 2: ");
    scanf("%d", &size2);
    inputArray(array2,size2);
    printf("\nMang 1 gom: ");
    outputArray(array1,size1);
    printf("\nMang 2 gom: ");
    outputArray(array2,size2);
    if(comparedArray(array1,size1,array2,size2) ==  1){
        printf("\nArray 1 bang Array 2");
    }else{
        printf("\nArray 1 khong bang Array 2");
    }

    compineArray(array1,&size1,array2,size2);
    compineArrayV2(array1,size1,array2,size2,totalArray,&totalSize);
    printf("\nsau khi gop mang:");
    outputArray(array1,size1);
    sortArray(array1,size1);
    printf("\nSau khi sap xep xong: ");
    outputArray(array1,size1);
    printf("\nSo lon nhat la %d", array1[size1 - 1]);
    printf("\nSo nho nhat la %d", array1[0]);
    return 0;
}
void inputArray(int array[], int size){
    for(int i = 0; i <= size -1; i++){
        printf("\nArray[%d]", i);
        scanf("%d", &array[i]);
    }
}
void outputArray(int array[], int size){
    for(int i = 0; i <= size -1; i++){
        printf("%6d", array[i]);
    }
}
int comparedArray(int array1[], int size1, int array2[], int size2){
    if(size1 == size2){
        for(int i = 0; i <= size1 - 1; i++){
            if(array1[i] != array2[i]){
                return 0;
            }
        }
        return 1;
    }else{
        return 0;
    }
}
int compineArray(int array1[], int* size1, int array2[], int size2){
    for(int i = 0; i <= size2 - 1; i++){
            array1[*size1] = array2[i];
            *size1 = *size1 + 1;
    }
}
int compineArrayV2(int array1[], int size1, int array2[], int size2,int totalArray[],int *totalSize){
    //duyet array 1
    for(int i = 0; i<= size1 - 1;i++){
        totalArray[*totalSize] = array1[i];
        *totalSize = *totalSize + 1;
    }
    //duyệt mảng 2
        for(int i = 0; i<= size2 - 1;i++){
        totalArray[*totalSize] = array2[i];
        *totalSize = *totalSize + 1;
    }
}
int sortArray(int array[], int size){
    for(int i = 0; i <= size - 2;i++){
        for(int j = i + 1; j <= size - 1; j++){
            if(array[i] > array[j]){
                int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
            }
        }
    }
}

