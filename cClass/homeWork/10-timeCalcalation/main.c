#include <stdio.h>
#include <stdlib.h>

int main()
{
    //nhập bào số giây, và trả ra cho anh số giờ phút giây
    //theo format sau: HH:MM:SS
    //59s=> 00:00:59//60
    //123=> 00:02:03//3600
    //3723=>01:02:03
    int number;
    int min = 0;
    int hour = 0;
    printf("\nTime: ");
    scanf("%d", &number);
    if(number >= 3600){
        hour = number / 3600;
        number = number % 3600;
    }
    if( number >= 60){
        min = number / 60;
        number = number % 60; //sss
    }
    if(hour < 10){
        printf("\n0%d", hour);
    }else{
        printf("\n%d", hour);
    }
    if(min < 10){
        printf(":0%d", min);
    }else{
        printf(":%d", min);
    }
    if(number < 10){
        printf(":0%d", number);
    }else{
        printf(":%d", number);
    }

    return 0;
}
