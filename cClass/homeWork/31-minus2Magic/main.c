#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[][MAX], int* sizeX, int* sizeY);
void outputArray(int array[][MAX], int sizeX, int sizeY);
int minusArray(int array[][MAX], int sizeX, int sizeY);
int main()
{
    int array1[MAX][MAX];
    int sizeX1 = 0;
    int sizeY1 = 0;
    int array2[][MAX];
    int sizeX2 = 0;
    int sizeY2 = 0;
    printf("\nNhap mang 1:");
    inputArray(array1,&sizeX1,&sizeY1);
    printf("\nNhap mang 2:");
    inputArray(array2,&sizeX2,&sizeY2);
    return 0;
}
void inputArray(int array[][MAX], int* sizeX, int* sizeY){
    printf("\nSize y : ");
    scanf("%d", sizeY1);
    printf("\nSize x : ");
    scanf("%d", sizeX1);
    for(int i = 0; i <= *sizeY - 1; i++){
        for(int j = 0; j <= *sizeX - 1; j++){
            printf("\nArray[%d][%d]: ", i,j);
            scanf("%d", &array[i][j]);
        }
    }
}
void outputArray(int array[][MAX], int sizeX, int sizeY){
    for(int i = 0; i <= sizeY - 1; i++){
        printf("\n");
        for(int j = 0; j <= sizeX - 1; j++){
            printf("\n%4d", array[i][j]);
        }
    }
}
int minusArray(int array1[][MAX], int sizeX1, int sizeY1, int array2[][MAX], int sizeX2, int sizeY2){
    if(sizeX1 == sizeX2 && sizeY1 == sizeY2){
        for(int i = 0; i <= sizeY - 1; i++){
            for(int j = 0; j <= sizeX - 1; j++){
                array1[i][j] = array1[i][j] - array2[i][j];
            }
        }
        return 1;
    }else{
        return 0;
    }
}
