#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main()
{
    //nhập vào 3 số
    //ax^2 + bx + c
    float a,b,c;
    printf("a= ");
    scanf("%f", &a);
     printf("b= ");
    scanf("%f", &b);
     printf("c= ");
    scanf("%f", &c);

    float d = b*b - 4*a*c;
    if(a == 0){
        if(b == 0){
            if(c == 0){
                printf("\nPhuong trinh vo so nghiem");
            }else{
                printf("\nPhuong trinh vo nghiem");
            }
        }else{
            printf("\nPhuong trinh co 1 nghiem %.2f", (-c)/b);
        }
    }else{
        if(d < 0){
            printf("\nPhuong trinh vo nghiem");
        }else if(d > 0){
            printf("\nPhuong trinh co 2 nghiem");
            printf("\nx1 = %f",((-b)+sqrt(d))/(2*a));
            printf("\nx2 = %f",((-b)-sqrt(d))/(2*a));
        }else{
            printf("\nPhuong trinh co nghiem kep");
            printf("\nx1 == x2 = %f", (-b)/(2*a));
        }
    }
    return 0;
}
