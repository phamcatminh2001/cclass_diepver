#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include<isotream>
const int MAX = 100;
void menu(void);
int inputArray(int array[], int size);
int outputArray(int array[],int size);
int findBiggestinArray(int array[],int size);
void sortArray(int array[],int size);
int averageOfNumber(int array[], int size);
float averageMultiplication(int array[], int size);
int main()
{
    //viet menu nhap 1 mang, in ra mang do
    //tim gtri lon nhat trong mang, tim gtri lon nhi trong mang, tim gtri nho nhat trong mang
    //tim gtri tbcong cua cac phan tu trong mang, trung binh nhan.
    int choose;
    char buffer;
    int array[MAX];
    int size = 0;
    do{
        menu();
        do{
            printf("\nNhap so ban muon chon: ");
            scanf("%d", &choose);
            scanf("%c", &buffer);
            if(buffer != 10){
                printf("\nBan nhap sai roi");
            }
        }while(buffer != 10);
        switch(choose){
            case 1:{
                printf("\nChuong trinh nhap mang");
                printf("\nNhap size cua mang: ");
                scanf("%d", &size);
                inputArray(array, size);
                break;
            }
            case 2:{
                if(size <= 0){
                    printf("\nMang Rong");
                }else{
                    printf("\nMang: ");
                    outputArray(array,size);
                }
                break;
            }
            case 3:{
                printf("\nGTLN1 trong mang");
                //printf("\nSo lon nhat trong mang la: %d", findBiggestinArray(array,size));
                sortArray(array,size);
                printf("\nPhan tu lon nhat co gia tri la: %d", array[size - 1]);
                break;
            }
            case 4:{
                printf("\nGTLN2 trong mang");
                sortArray(array,size);
                printf("\nPhan tu lon nhi co gia tri la: %d", array[size - 2]);
                break;
            }
            case 5:{
                printf("\nGTNN trong mang");
                sortArray(array,size);
                printf("\nPhan tu nho nhat co gia tri la: %d", array[0]);
                break;
            }
            case 6:{
                printf("\nTrung binh cong");
                printf("\nTrung binh cong cua chung la: %d ",averageOfNumber(array,size));
                break;
            }
            case 7:{
                printf("\nTrung binh nhan");
                printf("\nTrung binh nhan cua chung la: %d", averageMultiplication(array,size));
                break;
            }
            case 8:{
                printf("\nSee you again");
                break;
            }
            default:{
                printf("\nBan nhap sai roi, vui long nhap lai");
            }
        }
    }while(choose != 8);
    return 0;
}
void menu(void){
    printf("\nMenu xu ly mang");
    printf("\n1. Nhap mang");
    printf("\n2. Xuat mang");
    printf("\n3. GTLN1 trong mang");
    printf("\n4. GTLN2 trong mang");
    printf("\n5. GTNN trong mang");
    printf("\n6. Trung binh cong");
    printf("\n7. Trung binh nhan");
    printf("\n8. Quit");
}
int inputArray(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("\narray[%d]  = ",i);
        scanf("%d", &array[i]);
    }
}
int outputArray(int array[],int size){
    for(int i = 0; i<= size -1 ; i++){
        printf("%5d", array[i]);
    }
}
int findBiggestinArray(int array[],int size){
    int max = array[0];
    for(int i = 0; i<= size -1 ; i++){
        if(max < array[i]){
            max = array[i];
        }
    }
    return max;
}
void sortArray(int array[],int size){
    for(int i = 0; i<= size - 2;i++){
        for(int j = i+ 1 ; j <= size - 1;j++ ){
            if(array[i] > array[j]){
                //swap
                int tmp = array[j];
                array[j] = array[i];

                array[i] = tmp;
            }
        }
    }
}
int averageOfNumber(int array[], int size){
    int sum = 0;
    for(int i = 0; i <= size - 1; i++){
        sum = sum + array[i];
    }
    return sum/size;
}
float averageMultiplication(int array[], int size){
    float result;
    int sum = 1;
    float powD;
    powD = float(sum);

    for(int i = 0; i <= size - 1; i++){
        sum = sum * array[i];
    }

    printf("\nSum = %d", result);
    printf("\npow = %f", powD);
    return pow(result,powD);
}
