#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
int lenghString(char character[]);
int findCharInString(char character[]);
int countVowelInString(char character[]);
int sortString(char character[]);
int copyString(char characterTmp[], char character[]);
void rverseString(char character[], char characterTmp[]);
int swapString(char character[],char character2[]);
int findStrInStr(char character[]);
int countCharInString(char string1[], char key);
int checkAnagram(char character[], char character2[]);
int concatenateString(char character[], char character2[]);
int main()
{
    char character[MAX];
    char character2[MAX];
    int count = 0;
    char characterTmp[MAX];
    printf("\nNhap chuoi 1: ");
    gets(character);
    printf("\nNhap chuoi 2: ");
    gets(character2);
    //sortString(character);
    //printf("\nChuoi sau khi sap xep: %s", character);
    //printf("\nChuoi ban vua nhap la: %s", character);
    //printf("\nDo dai cua chuoi la %d", lenghString(character));
    //count = findCharInString(character);
    //printf("\nChu do xuat hien %d lan", count);
    //count = countVowelInString(character);
    //printf("\nNguyen am xuat hien trong chuoi %d lan", count);
    //count = lenghString(character) - countVowelInString(character);
    //printf("\nPhu am xuat hien trong chuoi %d lan", count);
    //copyString(characterTmp,character);
    //printf("\nSau sao chep: %s", characterTmp);
    //reverseString(character,characterTmp);
    //printf("\nSau khi dao chieu: %s", characterTmp);
    //swapString(character,character2);
    //printf("\n%s", character);
    //printf("\n%s", character2);
    //tim chuoi trong chuoi
    //if(findStrInStr(character) == 1){
    //    printf("\nCo trong chuoi");
    //}else{
    //   printf("\nKhong co trong chuoi");
    //}
    //if(checkAnagram(character,character2) == 1){
    //    printf("\nHai chuoi tuong duong nhau");
    //}else{
    //    printf("\nHai chuoi khong giong nhau");
    //}
    //concatenateString(character,character2);
    //printf("\n%s", character);
    return 0;
}
int lenghString(char character[]){
    int size = 0;
    for(; character[size] != 0; ){
        size = size + 1;
    }
    return size;
}
int findCharInString(char character[]){
    int count = 0;
    printf("\nNhap chu cai ban muon tim: ");
    char key;
    scanf("%c",&key);
    printf("\nVi tri xuat hien: ");
    for(int i = 0; i <= lenghString(character) - 1; i++){
        if(key == character[i]){
            printf("%5d", i);
            count++;
        }
    }
    return count;
}
int countVowelInString(char character[]){
    int count = 0;
    for(int i = 0; i <= lenghString(character) - 1; i++){
        if(character[i] == 'u' || character[i] == 'i' || character[i] == 'a' || character[i] == 'e' || character[i] == 'o'){
            count++;
        }
    }
    return count;
}
int sortString(char character[]){
    for(int i = 0; i <= lenghString(character) - 2; i++){
        for(int j = i + 1 ; j <= lenghString(character) - 1; j++){
            if(character[i] > character[j]){
                char tmp = character[i];
                    character[i] = character[j];
                    character[j] = tmp;
            }
        }
    }
}
int copyString(char characterTmp[], char character[]){
    int fakeLengh = 0;
    for(int i = 0; i <= lenghString(character); i++){
        characterTmp[fakeLengh] = character[i];
        fakeLengh++;
    }
}
void reverseString(char character[], char characterTmp[]){
    int fakeSize = 0;
    for(int i = lenghString(character) - 1; i >= 0; i--){
        characterTmp[fakeSize] = character[i];
        fakeSize++;
    }
    characterTmp[fakeSize] = 0;
}
int swapString(char character[],char character2[]){
    char strTmp[MAX];
    copyString(strTmp, character);
    copyString(character, character2);
    copyString(character2, strTmp);
}
int findStrInStr(char character[]){
    char findChar[MAX];
    printf("\nNhap chuoi ban can tim: %s", findChar);
    gets(findChar);
    for(int i = 0; i <= lenghString(character) - 1; i++){
        int y = i;
        for(int j = 0; j <= lenghString(findChar) - 1; j++){
            if(character[y] != findChar[j]){
                break;
            }
            y++;
            if(y - i == lenghString(findChar)){
                return 1;
            }
        }
    }
    return 0;
}
 /*   //check anagram
    //Diep
    //Diep
    //=> anagram
    //Diep
    //peid
    //=>anagram
    //ppei
    //peip
    //anagram
    //ppie
    //piie
    //=> khong phai anagram
    //2 chuoi duoc goi la anagram khi: 2 chuoi nay co cung kich thuoc
    //2 thang nay co so ky tu giong nhau*/
int checkAnagram(char character[], char character2[]){
    int count = 0;
    int count2 = 0;
    if(lenghString(character) != lenghString(character2)){
        return 0;
    }else{
        for(int i = 0; i <= lenghString(character) - 1;i++){
            count = countCharInString(character, character[i]);
            count2 = countCharInString(character2, character[i]);
            if(count != count2){
                return 0;
            }
        }
        return 1;
    }
}
int countCharInString(char string1[], char key){
    int count = 0 ;
    for(int i = 0;i<= lenghString(string1) - 1; i++){
        if(key == string1[i]){
            count++;
        }
    }
    return count;
}
int concatenateString(char character[], char character2[]){
    int size2 = 0;
    for(int i = lenghString(character); i <= lenghString(character) + lenghString(character2); i++){
        character[i] = character2[size2];
        size2++;
    }
}
//xin chaoba n  \0
//0123456789 10 11 size 8  3
//ban\0
//0123 size 3

//start 8 cua string1 lengthString1  size1 + size2
