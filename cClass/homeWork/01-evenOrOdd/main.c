#include <stdlib.h>

int main()
{
    //nhập vào 1 số nguyên, kiểm tra số đó có phải là chẳn hay lẽ, sau đó in ra thông báo
    // %: chia lấy dư
    //13%10 == 3 : true
    int number;
    printf("\nSo nguyen: ");
    scanf("%d", &number);
    if(number % 2 == 0){
        printf("so chan");
    }else{
        printf("\so le");
    }

    return 0;
}
