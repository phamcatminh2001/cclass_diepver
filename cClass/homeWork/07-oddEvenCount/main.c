#include <stdio.h>
#include <stdlib.h>

int main()
{
//nhập vào 1 khoản start--end
//in ra cac so chan , so le trong mang ,. và dem xem bao nhieu so
//1-10 = 10 -1 start < end
//co 5 so chan: 2 4 6 8 10
//co 5  so le: 1 3 5 7 9


    int start;//hardcode: khong hay, k dung voi 1 app
    int end;
    int evenCount= 0;
    int oddCount= 1;
    printf("\nStart: ");
    scanf("%d", &start);
    printf("\nEnd: ");
    scanf("%d", &end);

    if(start > end){
        int tmp;
        tmp = start;
        start = end;
        end = tmp;
    }
    printf("\n%d - %d", start,end);
    //in ra chan
    printf("\n so chan la:");
    for(int i = start; i <= end; i++){
        if(i % 2 == 0){
            printf("%4d", i);
            evenCount++;
        }
    }
    printf("\nco %d so chan", evenCount);
    printf("\nCac so le la: ");
    for(int i = start; i <= end; i++){
        if(i % 2 != 0){
            printf("%4d", i);
            oddCount++;
        }
    }
    printf("\n %d so le", oddCount);
    return 0;
}
