#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputNumber(int array[], int size);
void outputNumber(int array[], int size);
int checkNumberInArray(int array[], int size, int number,int indexArray[],int* indexSize);
int main()
{
    int array[MAX];
    int size = 0;
    int number;
    int indexArray[MAX];
    int indexSize = 0;
    int result = 0;
    printf("\nBan muon nhap bao nhieu so: ");
    scanf("%d", &size);
    inputNumber(array,size);
    printf("\nMang vua nhap la: ");
    outputNumber(array,size);
    printf("\nNhap so ban muon tim trong mang: ");
    scanf("%d", &number);
    result = checkNumberInArray(array,size,number,indexArray,&indexSize);
    if(result != 0){
        printf("\nCac vi tri cua %d xuat hien trong mang la: ", number);
        outputNumber(indexArray,indexSize);
        printf("\ntong cong so %d xuat hien %d lan trong mang",number,result);
    }else{
        printf("\nKhong co so do trong mang");
    }
    return 0;
}
void inputNumber(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("\nNhap vo ban oi array[%d]: ", i);
        scanf("%d", &array[i]);
    }
}
void outputNumber(int array[], int size){
    for(int i = 0; i <= size - 1; i++){
        printf("%4d", array[i]);
    }
}
int checkNumberInArray(int array[], int size, int number,int indexArray[],int* indexSize){
    int count = 0;
    for(int i = 0; i <= size - 1; i++){
        if(number == array[i]){
            count++;
            indexArray[*indexSize] = i;
            *indexSize= *indexSize + 1;
        }
    }
    return count;
}
