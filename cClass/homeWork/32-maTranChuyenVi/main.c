#include <stdio.h>
#include <stdlib.h>
const int MAX = 100;
void inputArray(int array[][MAX], int* sizeX, int* sizeY);
void outputArray(int array[][MAX], int sizeX, int sizeY);
void swapIndexMatrix(int array[][MAX], int* sizeX, int* sizeY);
int main()
{
    int array[MAX][MAX];
    int sizeX;
    int sizeY;
    inputArray(array,&sizeX,&sizeY);
    outputArray(array,sizeX,sizeY);
    swapIndexMatrix(array,&sizeX,&sizeY);
    printf("\nSau khi doi: ");
    outputArray(array,sizeX,sizeY);
    return 0;
}
void inputArray(int array[][MAX], int* sizeX, int* sizeY){
    printf("\nSize y : ");
    scanf("%d", sizeY);
    printf("\nSize x : ");
    scanf("%d", sizeX);
    for(int i = 0; i <= *sizeY - 1; i++){
        for(int j = 0; j <= *sizeX - 1; j++){
            printf("\nArray[%d][%d]: ", i,j);
            scanf("%d", &array[i][j]);
        }
    }
}
void outputArray(int array[][MAX], int sizeX, int sizeY){
    for(int i = 0; i <= sizeY - 1; i++){
        printf("\n");
        for(int j = 0; j <= sizeX - 1; j++){
            printf("%4d", array[i][j]);
        }
    }
}
void swapIndexMatrix(int array[][MAX], int* sizeX, int* sizeY){
     if(*sizeY > *sizeX){
        for(int i = 1; i <= *sizeY - 1; i++ ){
            for(int j = 0;j <= i - 1; j++){
                int tmp = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = tmp;
            }
        }

        int tmp = *sizeX ;
        *sizeX = *sizeY;
        *sizeY = tmp;
     }else{
        for(int i = 1; i <= *sizeX - 1; i++ ){
            for(int j = 0;j <= i - 1; j++){
                int tmp = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = tmp;
            }
        }

        int tmp = *sizeX ;
        *sizeX = *sizeY;
        *sizeY = tmp;
     }
}
/*
9 0
8 7  -> 9 8 6 8
6 4     1 7 4 3
        0 0 0 0
        0 0 0 0

        9868
        1743
*/
